 Module.modify :MUFTO_Reports_filters_setters_clients do
  methods set_client: <<-'METHOD'
        def self.set_client params
          okpo_arr = []
          if params[:shipper_client]
            params[:shipper_client].each do |item|
              if item[:client_id]
                okpo_arr << item[:client_id]
              else
                orm_client =User::ORM.get User::UFTO_ORM.default_binary_name
                begin
                  orm_client.execute do
                    param :sh , item[:shipper_id]
                    query 'select okpo_etran from orm_ufto_klient
                          where cast (ID_KLIENT_ETRAN as int ) in (
                          select org_id from ORM_UFTO_KLIENT_SHIPPER where HL_ID in($sh))'
                    row_result do |k|
                      okpo_arr << k["OKPO_ETRAN"]
                    end
                  end
                ensure
                  orm_client.done
                end
              end
            end
          else
            if params[:consignee_client]
              params[:consignee_client].each do |item|
                if item[:client_id]
                  okpo_arr << item[:client_id]
                else
                  orm_client =User::ORM.get User::UFTO_ORM.default_binary_name
                  begin
                    orm_client.execute do
                      param :sh , item[:consignee_id]
                      query 'select okpo_etran from orm_ufto_klient
                          where cast (ID_KLIENT_ETRAN as int ) in (
                          select org_id from ORM_UFTO_KLIENT_SHIPPER where HL_ID in($sh))'
                      row_result do |k|
                        okpo_arr << k["OKPO_ETRAN"]
                      end
                    end
                  ensure
                    orm_client.done
                  end
                end
              end
            else
              if params[:client]
                params[:client].each do |item|
                  okpo_arr << item[:client_id]
                end
              end
            end
          end
          return okpo_arr
        end
    METHOD
end
