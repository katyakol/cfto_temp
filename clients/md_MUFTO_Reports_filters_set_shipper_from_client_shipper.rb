 Module.modify :MUFTO_Reports_filters_setters_clients do
  methods set_shipper_from_client_shipper: <<-'METHOD'
      def self.set_shipper_from_client_shipper client_shipper
        client_shipper_arr = []
        client_shipper.each {|hash| client_shipper_arr << hash[:shipper_id]} if client_shipper && client_shipper.any?
        return client_shipper_arr
      end
    METHOD
end
