 Module.modify :MUFTO_Reports_filters_setters_clients do
  methods set_consignee_from_client_consignee: <<-'METHOD'
      def self.set_consignee_from_client_consignee client_consignee
        client_consignee_arr = []
        if client_consignee && client_consignee.any?
          client_consignee.each {|hash| client_consignee_arr << hash[:consignee_id]}
        end
        return client_consignee_arr
      end
    METHOD
end
