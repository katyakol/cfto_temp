 Module.modify :MUFTO_Reports_rep_3_2_1_1 do
  methods getData: <<-'METHOD'
      def self.getData params
        # @params: period, kind_of_shipping, group_freight, unit_measure
        filters = User::MUFTO_Reports.get_report_filters params        
        sql_query = '
          select year, kind_of_shipping, gr_freight_co11, 
          sum(weight), sum(car_quantity), sum(container_quantity), sum(freight_charge),
          sm.gg_name gg_name,
        '
        if filters[:period_type] == "day"
          table = 'orm_ufto_shipping_doc'         
          period_param = 'substr(day,0,2) day'
        end
        if filters[:period_type] == "month"
          table = 'orm_ufto_shipping_doc_month'   
          period_param = 'substr(month,6,2) month'
        end
        sql_query += period_param + ' from ' + table
        sql_query += '
          left join (
            select etran_code, kih_code
            from orm_ufto_gr_fr_mapping
            where etran_code in(#bind($gr_freight))
          ) mapp
          on mapp.kih_code = gr_freight_co11
          left join orm_ufto_sum_freight sm
          on sm.gg_number = mapp.etran_code
          where year in($base_year, $comp_year)
          and cast(substr(month,6,2) as int) in(#bind($monthes))
          and freight_code not in(421, 422, 423)
          and gr_freight_co11 in (mapp.kih_code)
        '            
        sql_query += 'and cast(substr(day,4,2) as int) in(#bind($monthes)) and cast(substr(day,0,2) as int) in(#bind($days))' if filters[:period_type] == "day"
        sql_query += 'group by year, '
        sql_query += 'substr(month,6,2)'  if filters[:period_type] == "month"
        sql_query += 'substr(day,0,2)'    if filters[:period_type] == "day"
        sql_query += ',kind_of_shipping, gr_freight_co11,sm.gg_name'
        begin
          orm_data_t =User::ORM.get User::UFTO_ORM.default_binary_name
          report = []
          orm_data_t.execute do
            param :gr_freight, filters[:group_freight]  if filters[:group_freight]
            param :base_year, filters[:base_year]       if filters[:base_year]
            param :comp_year, filters[:compare_year]    if filters[:compare_year]  
            param :monthes, filters[:month]             if filters[:month]
            param :days, filters[:day]                  if filters[:day]
            query sql_query
            row_result do |item|
              report << item
            end #row_result
            report.replace( User::MUFTO_Reports.get_report_hash_array report )
          end
        ensure
          orm_data_t.done
        end
        if report.any?
          report.replace(User::MUFTO_Reports_selectors.select_from_report( report, filters ))
          report.replace(User::MUFTO_Reports_rep_3_2_2_2.counting_report(report,filters[:period_type], filters[:representation], params[:period][:day])) if report.size > 1
          if filters[:compare_year] != 0
            if report.size > 1
              if filters[:period_type] == "month"
                report.sort_by!{|h| [h[:kind_of_shipping],h[:group_freight_id],h[:year]]} if filters[:representation] == "allPeriod"
                report.sort_by!{|h| [h[:kind_of_shipping],h[:group_freight_id],h[:month_id],h[:year]]} if filters[:representation] == "oneUnitPeriod"
              elsif filters[:period_type] == "decade" || filters[:period_type] == "day"
                report.sort_by!{|h| [h[:kind_of_shipping],h[:group_freight_id],h[:year]]} if filters[:representation] == "allPeriod"
                report.sort_by!{|h| [h[:kind_of_shipping],h[:group_freight_id],h[:day],h[:year]]} if filters[:representation] == "oneUnitPeriod"
              end
            end
            report.replace(User::MUFTO_Reports_counters.count_diff_proc(report, filters[:base_year], filters[:compare_year], filters[:period_type], filters[:representation]))
          end
        end
        return report
      end
    METHOD
  end
  