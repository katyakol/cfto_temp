 Module.modify :MUFTO_Reports_filters_getters_freights do
  methods get_group_freight: <<-'METHOD'
    def self.get_group_freight
      gr_freight_arr = []
      orm_item =User::ORM.get User::UFTO_ORM.default_binary_name
      begin
        orm_item.execute do

          query 'SELECT gg_name, gg_number
                FROM ORM_UFTO_SUM_FREIGHT
                where recdateend >= sysdate and gg_number != 0
                '
          row_result do |item|
            gr_freight_arr << {
              :group_freight => item["GG_NAME"] ,
              :group_freight_id => item["GG_NUMBER"].to_i
            }
          end
        end
      ensure
        orm_item.done
      end
      gr_freight_arr.sort_by!{|h| h[:group_freight]}
      return gr_freight_arr
    end
  METHOD
end