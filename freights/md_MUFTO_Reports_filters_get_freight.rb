 Module.modify :MUFTO_Reports_filters_getters_freights do
  methods get_freight: <<-'METHOD'
    def self.get_freight
      freight_arr = []
      orm_item =User::ORM.get User::UFTO_ORM.default_binary_name
      begin
        orm_item.execute do
          query 'select substr(FR_CODE_ETSNG,1,3) fr_code, FR_NAME
                  from ORM_UFTO_FREIGHT
                  where ALPH_EXIST = 0 and FR_CODE_ETSNG != 0 and recdateend >= sysdate'
          row_result do |item|
            freight_arr << {
              :freight => item["FR_NAME"],
              :freight_id => item["FR_CODE"].to_i,
            }
          end
        end
      ensure
        orm_item.done
      end
      freight_arr.sort_by!{|h| h[:freight]}
      return freight_arr
    end
  METHOD
end