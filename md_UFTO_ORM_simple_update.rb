Module.modify :UFTO_ORM do

  methods :simple_update => <<-'METHOD'
    def self.simple_update(parameters, range = 5000)
      # Self-method. Обновление объектов
      #
      # @param [Hash] - хэш параметров, содержащий:
      #    :data => массив хэшей типа {:атрибут => значение}
      #    :userclass => UserClass, для которого регистрируются значения
      #    :context => ORM-context
      #    :pk => PrimaryKey
      #    :external_db => имя внешнего подключения, если надо
      # @param range [Numeric] - разбиение :data на блоки, по умолчанию 5000
      #
      # @return [Hash] - хэш, содержащий:
      #    :in => кол-во объектов во входном массиве
      #    :to_commit => кол-во объектов для сохранения (в случае ошибки: -1)
      #    :commited => кол-во сохраненных объектов (в случае ошибки: -1)
      #
      result = {:in => 0, :to_commit => 0, :commited => 0}

      data = parameters[:data]
      return result if data.nil?
      array = data.is_a?(Array) ? data : [data]
      return result if array.empty?
      result[:in] = array.size

      orm = User::ORM.get(parameters[:context])
      str_pk = parameters[:pk].to_s

      begin
        array.each_slice(range) do |subarray|
          subarray.each do |hash|
            orm.execute do 
              type parameters[:userclass]
              param :pk, hash[parameters[:pk]]
              query "#{str_pk} = $pk"
              result do |obj|
                hash.each do |attr_name, value|
                  unless (parameters[:userclass] == :OntK_UFTO_prediction_map && attr_name == :invoices_id)
                    obj.send("#{attr_name}=", value) if obj.respond_to?(attr_name.intern)
                  else
                    obj.send("#{attr_name}=", value.to_java_bytes) if obj.respond_to?(attr_name.intern)
                  end
                end
              end
              result[:to_commit] += 1
            end
          end
          orm.commit_changes
        end
      rescue => ex
        result[:to_commit] = -1
        User::err "UFTO_ORM.simple_update -- #{ex.class} :: #{ex.message}\n#{ex.respond_to?(:cause) ? ex.cause.class.to_s + " :: " + ex.cause.message : ""}#{ex.backtrace * "\n"}"
        orm.rollback_changes
      ensure
        orm.done if orm
      end
      
      result[:commited] = result[:to_commit]
      return result
    end
  METHOD
end
